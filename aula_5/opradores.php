<?php

#### OPERADOR TERNARIO ####
/*
CONDIÇÃO ? TRUE : FALSE;
*/

echo '<pre>';

$alunos = [];
$nomes = ['Mayara Manso', 'Mikael Assis', 'Leandro Ramos', 'Gabrielly Vieira', 'Matheus Oliveira'];
$bitbucket = ['https://bitbucket.org/myrmanso', 'https://bitbucket.org/myrmanso', 'https://bitbucket.org/myrmanso', 'https://bitbucket.org/myrmanso', 'https://bitbucket.org/myrmanso'];

for($l=0; $l<5; $l++){
    $alunos[$l]['nome'] = $nomes[$l];
    $alunos[$l]['bitbucket'] = $bitbucket[$l];
}

echo '<p>Resolução com Operador Ternário</p>
        <table border="1">
            <thead>
                <th>
                    Nome
                </th>
                <th>
                    Bitbucket
                </th>
            </thead>';

$cor = 'gray';

foreach ($alunos as $ind => $linha) {
    $cor = $cor == 'gray' ? 'white' : 'gray';
    echo "  <tr bgcolor='$cor'>
                <td>
                {$linha['nome']}
                </td>
                <td>
                {$linha['bitbucket']}
                </td>
            </tr>";
}

echo '</table>';

echo '<p>Resolução com If</p>
        <table border="1">
            <thead>
                <th>
                    Nome
                </th>
                <th>
                    Bitbucket
                </th>
            </thead>';

$cor = 'gray';

foreach ($alunos as $ind => $linha) {
    if($cor == 'gray'){
        $cor = 'white';
    }
    else{
        $cor = 'gray';
    }

    echo "  <tr bgcolor='$cor'>
                <td>
                {$linha['nome']}
                </td>
                <td>
                {$linha['bitbucket']}
                </td>
            </tr>";
}

echo '</table>';


##### OPERADORES === E !=== #####
$nome = 'Mayara Cristina Manso';
$posicao = strpos($nome, 'Mayara');

if($posicao !== false){
    echo "encontrado na posição $posicao!!";
}else{
    echo "Não encontrado";
}
