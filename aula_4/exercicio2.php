<?php
$alunos[0]['nome'] = 'Mayara Manso';
$alunos[0]['bitbucket'] = 'https://bitbucket.org/myrmanso';
$alunos[1]['nome'] = 'Mikael Assis';
$alunos[1]['bitbucket'] = 'https://bitbucket.org/mkrodrigues';
$alunos[2]['nome'] = 'Leandro Ramos';
$alunos[2]['bitbucket'] = 'https://bitbucket.org/leandro-silva-r';
$alunos[3]['nome'] = 'Gabrielly Vieira';
$alunos[3]['bitbucket'] = 'https://bitbucket.org/gabievs';
$alunos[4]['nome'] = 'Matheus Oliveira';
$alunos[4]['bitbucket'] = 'https://bitbucket.org/myrmanso';

echo '<p>Resolução com While</p>
        <table border="1">
            <thead>
                <th>
                    Nome
                </th>
                <th>
                    Bitbucket
                </th>
            </thead>';

$i = 0;

while ($i < count($alunos)) {
    echo "  <tr>
                <td>
                {$alunos[$i]['nome']}
                </td>
                <td>
                {$alunos[$i]['bitbucket']}
                </td>
            </tr>";
    $i++;
}

echo '</table>';
