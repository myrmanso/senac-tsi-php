<?php
#VERIFICA TODAS AS FUNÇÕES QUE TEM NO PHP
//  phpinfo();

// var_dump($_SERVER);

// echo "Seu IP é: {$_SERVER["REMOTE_ADDR"]}" ; // - consegue verificar o ip da máquina

#OUTRA MANEIRA
/*
$alunos = array( 0 => array('nome' => 'Mayara Manso',
                            'bitbucket' => 'https://bitbucket.org/myrmanso'),
                 1 => array( 'nome' => 'Mikael Silva',
                             'bitbucket' => 'https://bitbucket.org/myrmanso'));
*/

echo '<pre>';
/*
$alunos[0]['nome'] = 'Mayara Manso';
$alunos[0]['bitbucket'] = 'https://bitbucket.org/myrmanso';
$alunos[1]['nome'] = 'Mikael Assis';
$alunos[1]['bitbucket'] = 'https://bitbucket.org/myrmanso';
$alunos[2]['nome'] = 'Leandro Ramos';
$alunos[2]['bitbucket'] = 'https://bitbucket.org/myrmanso';
$alunos[3]['nome'] = 'Gabrielly Vieira';
$alunos[3]['bitbucket'] = 'https://bitbucket.org/myrmanso';
$alunos[4]['nome'] = 'Matheus Oliveira';
$alunos[4]['bitbucket'] = 'https://bitbucket.org/myrmanso';

var_dump($alunos);
*/

###################  ESTRUTURA DE LOOP / LAÇO  ###################

// while(condição){ }

//for($i = 0; $i >3; $i++){estrutura do laço}

//do{ações }while(condição)






