<?php
echo '<pre>';

$alunos = [];
$nomes = ['Mayara Manso', 'Mikael Assis', 'Leandro Ramos', 'Gabrielly Vieira', 'Matheus Oliveira'];
$bitbucket = ['https://bitbucket.org/myrmanso', 'https://bitbucket.org/myrmanso', 'https://bitbucket.org/myrmanso', 'https://bitbucket.org/myrmanso', 'https://bitbucket.org/myrmanso'];

for($l=0; $l<5; $l++){
    $alunos[$l]['nome'] = $nomes[$l];
    $alunos[$l]['bitbucket'] = $bitbucket[$l];
}

// var_dump($alunos);

##### EXPLICAÇÂO PROFESSOR #####
echo '<table border="1">
        <thead>
            <th>
                Nome
            </th>
            <th>
                Bitbucket
            </th>
        </thead>';

for($i=0; $i < count($alunos); $i++){
    echo "  <tr>
                <td>
                {$alunos[$i]['nome']}
                </td>
                <td>
                {$alunos[$i]['bitbucket']}
                </td>
            </tr>";
};

echo '</table>';

//AGORA COM FOREACH :-)
/*
foreach (VARIAVEL as INDICE => VALOR) {
    # code...
}
*/
echo '<p>Resolução com Foreach</p>
        <table border="1">
            <thead>
                <th>
                    Nome
                </th>
                <th>
                    Bitbucket
                </th>
            </thead>';


foreach ($alunos as $ind => $linha) {
    echo "  <tr>
                <td>
                {$linha['nome']}
                </td>
                <td>
                {$linha['bitbucket']}
                </td>
            </tr>";
}

echo '</table>';
