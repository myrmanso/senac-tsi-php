<?php
echo "<pre>";

/************************************/
//comentário de linha
/*
comentário de bloco
*/
# comentario de linha, pouco usado 
/***************************************/

//CONSTANTE
//define('variavel', valor)
define('QTD_PAGINAS', 10);

echo "valor da minha contante é: " . QTD_PAGINAS; 

//VARIAVEL PARA PASSAR VALOR PARA UMA CONSTANTE

$ip_do_banco = '192.168.45.12';

define ('IP_DO_BANCO', $ip_do_banco); //define uma constante

echo "\nO IP do SGDB é: " . IP_DO_BANCO;


//constantes mágicas
echo "\nEstou na linha: " . __LINE__;
echo "\nEstou na linha: " . __LINE__;
echo "\nEste é meu arquivo: " .__FILE__;


//para depurar código
echo "\n\n";
var_dump($ip_do_banco);


/* VETORES */
$dias_da_semana = [
                    'dom',
                    'seg',
                    'ter',
                    'qua',
                    'qui',
                    'sex',
                    'sab',
                ];
echo "\n\n";


var_dump($dias_da_semana);


# destroi a varaiavel
unset($dias_da_semana);





//outra aneira de declarar
$dias_da_semana[0] = 'dom';
$dias_da_semana[1] = "seg";
$dias_da_semana[2] = 'ter';
$dias_da_semana[3] = 'quar';
$dias_da_semana[4] = 'qui';
$dias_da_semana[5] = 'sex';
$dias_da_semana[6] = 'sab';

echo "\n\n";
var_dump($dias_da_semana);

# destroi a varaiavel
unset($dias_da_semana);


//outra maneira usando a função array

$dias_da_semana = array(0 => 'dom',
                        1 => 'seg',
                        2 => 'ter',
                        3 => 'qua',
                        4 => 'qui',
                        5 => 'sex',
                        6 => 'sab');


echo "\n\n";
var_dump($dias_da_semana);


echo "</pre>";
